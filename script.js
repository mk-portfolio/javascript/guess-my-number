"use strict";

let secretNumber = Math.trunc(Math.random() * 20)+1;
let score = 20;
let highScore = 0;


// DRY function for ".message"
const displayMessage = message => {
    document.querySelector(".message").textContent = message;
}

// DRY function for ".score"
const displayScore = score => {
    document.querySelector(".score").textContent = score;
}

// Function for Decreasing the Score
let guessScore = () => {
    score -= 1;
    displayScore(score);
}


// Event for Check Button
document.querySelector(".check").addEventListener("click", () => {
    
    const guess = Number(document.querySelector(".guess").value);
    console.log(guess, typeof guess);

    if(!guess){ // If Empty String on Input Field

        displayMessage("⛔ No Number!");

    } else if(guess === secretNumber){ // Correct Guess

        displayMessage("🎉 Correct Number!");

        document.querySelector(".number").textContent = secretNumber;

        /* Manipulating CSS Style
            - it could be HTML Element | ID | Classes
            - then .style.CSSPoperty = "value";
            Note:
                - Use camelCase if ther is two word for CSS Property
                - Always put the Value in a string

            i.e.:
                document.querySelector("HTML ELement).style.CSSPoperty = "value";

        */
        // Changing Background
        document.querySelector("body").style.backgroundColor = "#60b347";

        // Changing Width of Hidden Number
        document.querySelector(".number").style.width = "30rem";

        // Display HighScore
        if(score > highScore){

            highScore = score;

            document.querySelector(".highScore").textContent = highScore;

        }

    } else if (guess !== secretNumber){ // Wrong Guess

        if(score > 1){

            displayMessage(guess > secretNumber ? "📈 Too High!" : "📉 Too Low!");

            guessScore();

        } else {

            displayMessage("💥 You lost the game!");
            displayScore(0);

        }
    }

});


// Event for Resetting the Score, Guess Input and SecretNumber
document.querySelector(".again").addEventListener("click", () => {

    let guess = Number(document.querySelector(".guess").value);
    score = 20; // Reassign the variable Score to "20"

    // Reset the message to original value
    displayMessage("Start guessing..."); 

    if(guess === secretNumber){
        // ReAssigning the secretNumber if Guess is Correct
        secretNumber = Math.trunc(Math.random() * 20)+1;

        // Reset the Guess input to Empty string
        guess = document.querySelector(".guess").value = "";

        // Reset the Score to "20"
        displayScore(score)

        // Reset the Background-Color
        document.querySelector("body").style.backgroundColor = "#222";

        // Changing Width & Content of Hidden Number
        document.querySelector(".number").textContent = "?";
        document.querySelector(".number").style.width = "15rem";
        
    } else {

        // ReAssigning the secretNumber
        secretNumber = Math.trunc(Math.random() * 20)+1;

        // Reset the Score to "20"
        displayScore(score)

        // Reset the Guess input to Empty string
        guess = document.querySelector(".guess").value = "";

    }
});